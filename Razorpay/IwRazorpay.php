<?php

namespace IwLaravel\Razorpay;

use App\Enums\PaymentGatewayVendorEnum;
use App\Models\Order;
use Illuminate\Support\Facades\Config;
use IwLaravel\Razorpay\DTO\IwRazorOrderDTO;
use IwLaravel\Razorpay\DTO\IwRazorOrderFetchPaymentsDTO;
use IwLaravel\Razorpay\DTO\IwRazorPaymentFetchDTO;
use Razorpay\Api\Api;

class IwRazorpay
{
    static $api;

    public static function initIfVilorPartnerPayment(Order $order)
    {
        if (PaymentGatewayVendorEnum::RazorPayVilorPartners->value == $order->PaymentGateway) {
            self::setPaymentGatewayToVilorPartners();
        }
    }

    public static function setPaymentGatewayToVilorPartners()
    {
        Config::set('env.CURRENT_PAYMENT_GATEWAY', PaymentGatewayVendorEnum::RazorPayVilorPartners->value);
    }

    // NOTE: hardcode for vilor partners berhad payment
    public static function getApi()
    {
        if (PaymentGatewayVendorEnum::RazorPayVilorPartners->value == config('env.CURRENT_PAYMENT_GATEWAY')) {
            return app(Api::class, [
                'key' => config('env.VILOR_PARTNERS_PAYMENT_RAZOR_KEY_ID'),
                'secret' => config('env.VILOR_PARTNERS_PAYMENT_RAZOR_SECRET')
            ]);
        }

        if (!self::$api) {
            self::$api = app(Api::class, [
                'key' => config('env.PAYMENT_RAZOR_KEY_ID'),
                'secret' => config('env.PAYMENT_RAZOR_SECRET')
            ]);
        }

        return self::$api;
    }

    public function validateWebhook($webhookSignature, $rawRequestBody)
    {
        try {
            // first try for Vilor Berhad account
            $webhookSecret = config('env.PAYMENT_RAZOR_WEBHOOK_SECRET');
            self::getApi()
                ->utility->verifyWebhookSignature(
                    $rawRequestBody,
                    $webhookSignature,
                    $webhookSecret
                );
        } catch (\Throwable $e) {
            // if error, then try it with Vilor Partners account
            $webhookSecret = config('env.VILOR_PARTNERS_PAYMENT_RAZOR_WEBHOOK_SECRET');
            self::getApi()
                ->utility->verifyWebhookSignature(
                    $rawRequestBody,
                    $webhookSignature,
                    $webhookSecret
                );
        }
    }

    public function createOrder(IwRazorOrderDTO $order): \Razorpay\Api\Order
    {
        $res = self::getApi()->order->create($order->data);
        return $res;
    }

    public function fetchPayment(?string $gatewayPaymentId = null): \Razorpay\Api\Payment
    {
        if (!$gatewayPaymentId) {
            throw new \IwLaravel\Exceptions\RazorPaymentIdNotFound('gatewayPaymentId is required');
        }

        $res = self::getApi()->payment->fetch($gatewayPaymentId);
        return $res;
    }

    public function fetchPaymentAndReturnDTO(string $gatewayPaymentId): IwRazorPaymentFetchDTO
    {
        $res = $this->fetchPayment($gatewayPaymentId);
        $paymentPayload = new IwRazorPaymentFetchDTO($res->toArray());

        return $paymentPayload;
    }

    public function fetchAllPaymentsOfAnOrder(string $gatewayOrderId): IwRazorOrderFetchPaymentsDTO
    {
        $res = self::getApi()->order->fetch($gatewayOrderId)->payments();
        // TODO: to handle multiple payments under an order
        $paymentDTO = new IwRazorOrderFetchPaymentsDTO($res->toArray());
        return $paymentDTO;
    }

    public function fetchLatestPaymentOfAnOrder(string $gatewayOrderId): IwRazorPaymentFetchDTO
    {
        return $this->fetchAllPaymentsOfAnOrder($gatewayOrderId)->latestPayment();
    }
}
