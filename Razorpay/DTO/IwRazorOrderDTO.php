<?php

namespace IwLaravel\Razorpay\DTO;

use IwLaravel\Helpers\IwCurrency;

class IwRazorOrderDTO
{
    public $data = [];

    public function __construct(
        int $amount, // in cents
        IwCurrency $currency,
        string $receipt, // Receipt number for your internal app reference

        int $first_payment_min_amount = -1, // minimum partial payment
        bool $partialPayment = false, // false: customer cannot make partial payment
        array $notes = [],
    ) {
        $res = [
            'amount' => $amount,
            'currency' => $currency,
            'partial_payment' => $partialPayment,
            'receipt' => $receipt,
        ];

        if (count($notes) > 0) {
            $res['notes'] = $notes;
        }

        if ($first_payment_min_amount > 0) {
            $res['first_payment_min_amount'] = $first_payment_min_amount;
        }

        $this->data = $res;
    }
}
