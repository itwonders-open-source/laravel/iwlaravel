<?php

namespace IwLaravel\Razorpay\DTO;

class IwRazorPaymentFetchDTO
{
    public string $id;
    public string $entity;
    public int $amount;
    public string $currency;
    // public int $base_amount = null;
    public string $status;
    public ?string $order_id = null;
    public $invoice_id = null;
    // to indicate if it's an international card or domestic card
    public ?bool $international = null;
    // method: card, netbanking, wallet, emi, upi
    public ?string $method = null;
    public ?int $amount_refunded = null;
    public ?string $refund_status = null;
    public ?bool $captured = null;
    public ?string $description = null;
    public ?string $card_id = null;
    public ?string $bank = null;
    public ?string $wallet = null;
    public ?string $vpa = null;
    public ?string $email = null;
    public ?string $contact = null;
    public ?array $notes = null;
    public ?int $fee = null;
    public ?int $tax = null;
    public ?string $error_code = null;
    public ?string $error_description = null;
    public ?string $error_source = null;
    public ?string $error_step = null;
    public ?string $error_reason = null;
    public ?array $acquirer_data = null;
    public ?int $created_at = null;


    public function __construct(
        array $attributes
    ) {
        foreach ($attributes as $key => $value) {
            if ($value !== null) {
                $this->{$key} = $value;
            }
        }
    }

    public function isCaptured(): bool
    {
        return 'captured' == $this->status;
    }
}
