<?php

namespace IwLaravel\Razorpay\DTO;

/**
 * store all the payments of an order
 */
class IwRazorOrderFetchPaymentsDTO
{
    // indicates total items
    public int $count = 0;
    public array $items = [];

    public function __construct($attr)
    {
        $this->count = $attr['count'];
        foreach ($attr['items'] as $item) {
            $this->items[] = new IwRazorPaymentFetchDTO($item);
        }
    }

    public function firstPayment(): IwRazorPaymentFetchDTO
    {
        return $this->items[0];
    }

    public function latestPayment(): IwRazorPaymentFetchDTO
    {
        $res = $this->items[0];
        for ($i = 1; $i < count($this->items); $i++) {
            if ($this->items[$i]->created_at > $res->created_at) {
                $res = $this->items[$i];
            }
        }
        return $res;
    }
}
