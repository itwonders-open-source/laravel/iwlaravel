<?php

namespace IwLaravel\Helpers;

use Illuminate\Support\Facades\Log;

class IwDebug
{
    public static function backtrace($skip = 1, $limit = 12, $skipVendor = true): array
    {
        $dump = ['IwDebug: '];

        $mydump = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $limit);
        for ($idx = $skip; $idx < count($mydump); $idx++) {
            $item = $mydump[$idx];
            if ($skipVendor && isset($item['file'])) {
                if (strpos($item['file'], 'vendor') !== false) {
                    continue;
                }
            }
            if (!isset($item['file'])) {
                $dump[] = $item['class'] . "::" . $item['function'];
            } else {
                $dump[] = $item['file'] . "::" . $item['function'];
            }
        }
        return $dump;
    }

    public static function logBacktrace($skip = 2, $limit = 12, $skipVendor = true)
    {
        $arrDump = self::backtrace($skip, $limit);
        Log::debug(implode(PHP_EOL, $arrDump));
    }
}
