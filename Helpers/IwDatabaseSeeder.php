<?php

namespace IwLaravel\Helpers;

use Illuminate\Support\Facades\DB;

class IwDatabaseSeeder
{
    /**
     * To make seeder always work by first truncating the table
     *
     * @param array|string $classes array of Model classes
     * @return void
     *
     * @example
     * use Itwonders\Helpers\IwDatabaseSeeder;
     *
     * IwDatabaseSeeder::truncate([User::class, Role::classs]);
     */
    public static function truncate($classes)
    {
        if (is_string($classes)) {
            $classes::truncate();
        } elseif (\is_array($classes)) {
            if (!DB::connection() instanceof \Illuminate\Database\SQLiteConnection) {
                DB::statement('SET FOREIGN_KEY_CHECKS=0;');

                foreach ($classes as $myclass) {
                    $myclass::truncate();
                }

                DB::statement('SET FOREIGN_KEY_CHECKS=1;');
            }
        }
    }
}
