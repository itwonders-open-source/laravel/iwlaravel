<?php

namespace IwLaravel\Helpers;

use Illuminate\Support\Facades\Hash;

class IwPassword
{
    public static function generate(): string
    {
        $length = 10; // The desired length of the password
        $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_'; // The character set to use for the password
        $password = '';
        $charset_length = strlen($charset);

        for ($i = 0; $i < $length; $i++) {
            $password .= $charset[random_int(0, $charset_length - 1)];
        }

        // PASSWORD_DEFAULT will use bcrypt algorithm
        $hashed_password = Hash::make($password);

        return $hashed_password;
    }
}
