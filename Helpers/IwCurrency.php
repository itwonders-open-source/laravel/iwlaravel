<?php

namespace IwLaravel\Helpers;


enum IwCurrency: string
{
    case MYR = 'MYR';
    case SGD = 'SGD';

    public static function get(string $value): IwCurrency
    {
        if ($value == 'RM') {
            return IwCurrency::MYR;
        }

        return IwCurrency::from($value);
    }
}
