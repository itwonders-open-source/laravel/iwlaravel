<?php

namespace IwLaravel\Helpers;

use Hashids\Hashids;

class IwHashId
{
    public static $hashids8;
    public static $hashids5;

    public function getEncoded8($data): string
    {
        if (!self::$hashids8) {
            self::$hashids8 = new Hashids(config('env.HASHID_SALT'), 8);
        }
        return self::$hashids8->encode($data);
    }

    public function getDecoded8(string $data): array
    {
        if (!self::$hashids8) {
            self::$hashids8 = new Hashids(config('env.HASHID_SALT'), 8);
        }
        return self::$hashids8->decode($data);
    }

    public function getDecoded8Id(string $data): ?int
    {
        $res =  $this->getDecoded8($data);
        if (count($res) > 0) {
            return $res[0];
        }
        return null;
    }

    public function getEncoded5($data): string
    {
        if (!self::$hashids5) {
            self::$hashids5 = new Hashids(config('env.HASHID_SALT'), 5);
        }
        return self::$hashids5->encode($data);
    }

    public function getDecoded5(string $data): array
    {
        if (!self::$hashids5) {
            self::$hashids5 = new Hashids(config('env.HASHID_SALT'), 5);
        }
        return self::$hashids5->decode($data);
    }

    public function getDecoded5Id(string $data): ?int
    {
        $res = $this->getDecoded5($data);
        if (count($res) > 0) {
            return $res[0];
        }
        return null;
    }
}
