<?php

namespace IwLaravel\Iface;

interface IwCollectionIface
{
    public function toArray(&$collection): array;
}
